from MainWindow import MainWindow,QApplication

import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

app = QApplication([])
window = MainWindow()
window.show()
app.exec()

