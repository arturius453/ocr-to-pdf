from PIL import Image
import pytesseract
import PyPDF2
import imutils
import cv2
import io
import numpy as np

FRAMES = 0
CAPTURED = False
FGBG = cv2.createBackgroundSubtractorMOG2()

def four_point_transform(image, pts):
    """
    Афінна прямокутна трансформація на основі виявлених координатів листа
    """
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    dst = np.array([
    	[0, 0],
    	[maxWidth - 1, 0],
    	[maxWidth - 1, maxHeight - 1],
    	[0, maxHeight - 1]], dtype = "float32")
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    return warped

    
def order_points(pts):
    """
    Сортування точок виявленного прямокутника для трансформації
    """
    rect = np.zeros((4, 2), dtype = "float32")
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    return rect

    
def scan(orig):
    """Сканування захопленого в кадрі листа"""
    gray = cv2.cvtColor(orig, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(gray, 75, 200)
    #cv2.imshow("Contours",edged)
    cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:5]
    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.01 * peri, True)
        if len(approx) == 4:
            cnt = approx
            warped = four_point_transform(orig, cnt.reshape(4, 2))
            #cv2.imshow("Scanned", warped)
            return warped
    #cv2.imshow("Contours",edged)
    return orig

        
def segmentate(frame, p_thrash=20, f_warmup=10):
    """
    Сегментація відео з метою виявлення кадрів з листом.
    Приймає відео покадрово.
    Чим вище p, тим чутливіше сегментатор до динамічних змін відео.
    Чим нижче p, тим чутливіше сегментатор при нединамічних змінах.
    Повертає два значення:
    1. Булеве значення, що кадр захопленно
    2. Захоплене трансформоване зображення або None
    """
    global CAPTURED
    global FRAMES
    orig = frame.copy()
    frame = imutils.resize(frame, width=600)
    mask = FGBG.apply(frame)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    (H, W) = mask.shape[:2]
    p = (cv2.countNonZero(mask) / float(W * H)) * 100
    FRAMES += 1
    if p < p_thrash and not CAPTURED and FRAMES > f_warmup:
        CAPTURED = True
        page = scan(frame)
        if page is not None:
            return True, page
    elif CAPTURED and p >= p_thrash:
        CAPTURED = False
    return False, None


def images_to_pdf(pdf_name, images,lang="eng"):
    """
    Перетворення списку зображень в searchable-pdf
    """
    config = f"-l {lang}"
    pdf_writer = PyPDF2.PdfFileWriter()
    for img in images:
        img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
        image = Image.fromarray(img)
        page = pytesseract.image_to_pdf_or_hocr(image, extension='pdf',
                config=config)
        pdf = PyPDF2.PdfFileReader(io.BytesIO(page))
        pdf_writer.addPage(pdf.getPage(0))
    with open(pdf_name+".pdf", "wb") as f:
        pdf_writer.write(f)


def images_to_text(images,lang="eng"):
    """
    Перетворення списку зображень в список розпізнаних текстів для кожної
    сторінки
    """
    result = []
    config = f"-l {lang}"
    for img in images:
        image = Image.fromarray(img)
        text = pytesseract.image_to_string(image,config=config)
        result.append(text)
    return result
