from PyQt5.QtCore import QRunnable
from real_time_ocr import *

class AnalyzeThread(QRunnable):
    def __init__(self, n):
        super().__init__()
        self.n = n
        self.toStop=False
        self.images=[]

    def run(self):
            to_pdf = []
            vs = cv2.VideoCapture(self.n) 
            while not self.toStop:
                (grabbed, frame) = vs.read()
                if frame is None:
                    break
           #     cv2.imshow("Frame", frame)
                res, page = segmentate(frame)
                if res:
                        to_pdf.append(page)

            vs.release()
            self.images=to_pdf
            print(images_to_text(to_pdf))
    def stop(self):
        self.toStop=True
        return self.images
