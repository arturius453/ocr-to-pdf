import cv2
from real_time_ocr import *

to_pdf = []

output = "output"


vs = cv2.VideoCapture(1)
while True:
    (grabbed, frame) = vs.read()
    if frame is None:
      break
    res, page = segmentate(frame)
    if res:
            to_pdf.append(page)
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

images_to_pdf("sample",to_pdf)
print(images_to_text(to_pdf))
cv2.destroyAllWindows()
vs.release()
