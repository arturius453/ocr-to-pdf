import cv2
from PyQt5 import uic
from PyQt5.QtCore import QThreadPool
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *
from PyQt5.QtWidgets import *

from AnalyzerThread import AnalyzeThread
from real_time_ocr import *


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # loading the ui file with uic module
        uic.loadUi("mainWindow.ui", self)
        self.fileButton.clicked.connect(lambda: self.fileBut_clicked())
        self.analyzeButton.clicked.connect(lambda: self.fAnalyzeBut_clicked())
        self.startStopBut.clicked.connect(lambda: self.vAnalyzeBut_clicked())
        self.camera_selector.currentIndexChanged.connect(self.select_camera)
        self.threadpool = QThreadPool()

        self.available_cameras = QCameraInfo.availableCameras()
  
        # if no camera found
        if not self.available_cameras:
            None
  
        # creating a QCameraViewfinder object
        self.viewfinder = QCameraViewfinder()
        self.tab_2.layout().addWidget(self.viewfinder)

        # adding items to the combo box
        self.camera_selector.addItems([camera.description()
                                  for camera in self.available_cameras])

  

    def fileBut_clicked(self):
       self.imagePath=QFileDialog.getOpenFileName(self,
               'Відкрити файл','./',
               "Image files (*.jpg *.png *.jpeg)")[0]

    def fAnalyzeBut_clicked(self):
        #print(self.tabWidget.currentIndex())
        if not hasattr(self,'imagePath'):
            QMessageBox(QMessageBox.Icon.Information,"Помилка", 
                    "Виберіть картинку").exec()
            return
        self.resText.clear()
        lang="eng" if self.language_selector.currentText()=="English" else 'ukr'
        image=cv2.imread(self.imagePath, cv2.IMREAD_COLOR)
        for str in images_to_text([image],lang):
            self.resText.append(str)

        cvImg = scan(image)
        Img = cv2.cvtColor(cvImg,cv2.COLOR_BGR2RGB)
        h,w,ch = Img.shape
        bytes_per_line = ch * w
        qtFormat = QImage(Img.data, w, h,
                                 bytes_per_line,QImage.Format_RGB888)
        pixmap = QPixmap.fromImage(qtFormat).scaled(640,400)
        self.resLabel.setPixmap(pixmap)

    def vAnalyzeBut_clicked(self):
        if (self.startStopBut.text()=='Старт'):
            self.startStopBut.setText('Стоп')
            curr_camera=self.available_cameras[self.camera_selector.currentIndex()]
            cam_id=int(curr_camera.deviceName().removeprefix("/dev/video"))
            self.worker=AnalyzeThread(cam_id)
            self.threadpool.start(self.worker)

        else:
            self.images=self.worker.stop()
            self.startStopBut.setText('Старт')
            
        
        
        #images_to_pdf("sample",to_pdf)

    def select_camera(self, i):
  
        # getting the selected camera
        self.camera = QCamera(self.available_cameras[i])
        print(self.available_cameras[i].deviceName())
  
        # setting view finder to the camera
        self.camera.setViewfinder(self.viewfinder)
  
        # setting capture mode to the camera
        #self.camera.setCaptureMode(QCamera.CaptureStillImage)
  
        # if any error occur show the alert
        print(self.camera.errorString())
  
        # start the camera
        self.camera.start()
  
 
        # getting current camera name
        self.current_camera_name = self.available_cameras[i].description()
  
        # initial save sequence
        self.save_seq = 0
  
